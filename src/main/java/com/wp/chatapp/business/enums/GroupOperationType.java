package com.wp.chatapp.business.enums;

public enum GroupOperationType {
    ADD_MEMBER,
    REMOVE_MEMBER
}
