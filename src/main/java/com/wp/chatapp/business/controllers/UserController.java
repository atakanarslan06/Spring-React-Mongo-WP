package com.wp.chatapp.business.controllers;

import com.wp.chatapp.business.dto.UserDto;
import com.wp.chatapp.business.services.UserService;
import com.wp.chatapp.dal.models.FriendshipRequest;
import com.wp.chatapp.dal.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;

    }

    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody UserDto userDto){
        String response = userService.createUser(userDto);
        return ResponseEntity.ok(response);
    }
    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateUser(@PathVariable String id, @RequestBody UserDto userDto){
        String response = userService.updateUser(id, userDto);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id){
        Optional<User> userOptional = userService.getUserById(id);
        return userOptional.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @PatchMapping("/{id}/activate")
    public ResponseEntity<String> activateUser(@PathVariable String id){
        String response = userService.activateUser(id);
        return ResponseEntity.ok(response);
    }

    @PatchMapping("/{id}/deactivate")
    public ResponseEntity<String> deactivateUser(@PathVariable String id){
        String response = userService.deactivateUser(id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/findByPhoneNumber/{phoneNumber}")
    public ResponseEntity<User> findByPhoneNumber(@PathVariable String phoneNumber){
        User user = userService.findByPhoneNumber(phoneNumber);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/sendFriendRequest/{receiverId}")
    public ResponseEntity<String> sendFriendRequest(@PathVariable String receiverId, @RequestBody Map<String, String> requestBody) {
        String senderId = requestBody.get("senderId");
        String response = userService.sendFriendRequest(senderId, receiverId);
        return ResponseEntity.ok(response);
    }


    @PostMapping("/handleFriendRequest/{requestId}/{userId}")
    public ResponseEntity<String> handleFriendRequest(
            @PathVariable String requestId,
            @PathVariable String userId,
            @RequestParam boolean accept
    ) {
        String response = userService.handleFriendRequest(requestId, userId, accept);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}/friends")
    public ResponseEntity<List<User>> getFriends(@PathVariable String id){
        List<User> friends = userService.getFriends(id);
        return ResponseEntity.ok(friends);
    }

    @GetMapping("/{id}/friendshipRequests")
    public ResponseEntity<List<FriendshipRequest>> getFriendshipRequests(@PathVariable String id){
        List<FriendshipRequest> friendshipRequests = userService.getFriendshipRequests(id);
        return ResponseEntity.ok(friendshipRequests);
    }
    @GetMapping("/me")
    public User getCurrentUser(@AuthenticationPrincipal UserDetails userDetails) {
        String userEmail = userDetails.getUsername();
        return userService.findByEmail(userEmail);
    }

    @PatchMapping("/deleteFriend")
    public ResponseEntity<String> deleteFriend(@RequestBody Map<String, String> requestBody) {
        String userId = requestBody.get("userId");
        String friendId = requestBody.get("friendId");

        String response = userService.deleteFriend(userId, friendId);
        return ResponseEntity.ok(response);
    }


}
